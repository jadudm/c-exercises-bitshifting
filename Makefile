clean:
	rm -f run_tests

tests: clean
	gcc -Os -c bitlib.c
	gcc -Os -o run_tests bitlib.o tests.c 

all: clean tests

