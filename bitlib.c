/**
 * @file bitlib.c
 * @author Matthew C. Jadud
 * @date October 1, 2014
 * 
 * A library of bit-shift operations that we will 
 * likely use later in our VM implementation.
 * 
 * */

#include "bitlib.h"

uint16_t shift_left_one (uint16_t number) {
  return number << 1;
}

uint16_t shift_left (uint16_t number, int places) {
  return number;
}

uint16_t shift (uint16_t number, int places, direction dir) {
  if (dir == left) {
      return 0;
  } else if (dir == right) {
      return 0;
  }
  
}
 
uint16_t zero_lower (uint16_t number) {
  printf("ZL: %X %X\n", number, (number & (uint16_t)0xFF00));
  
  return (uint16_t)(number & (uint16_t)0xFF00);
}

uint16_t zero_upper (uint16_t number) {
  return 0;
}

uint16_t zero (uint16_t number, byte b) {
  if (b == upper) {
    
  }
  
  return 0;
}

uint8_t get_byte (uint16_t number, byte b) {
  return 0;
}

bool is_bit_set (uint16_t number, int n) {
  return false;
}

/* First Challenge Implementation
 * ******************************
 * Add tests for both get_byte and is_bit_set.
 */

/* Second Challenge Implementation
 * *******************************
 * After you have completed implementations of the above functions,
 * circle back around and add in functions that shift right where
 * appropriate. 
 *
 * shift_one_right
 * shift_right
 *
 * You will need to modify bitlib.h to add declarations for
 * these functions.
 * 
 * You will need to modify tests.c to add tests for these functions.
 *
 * You will need to modify bitlib.c to add implementations for
 * each of these functions.
 *
 * If you are successful, you'll have more tools for your
 * virtual machine implementation.
 */